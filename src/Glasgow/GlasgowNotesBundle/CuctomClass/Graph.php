<?php

namespace Glasgow\GlasgowNotesBundle\CuctomClass;

class Graph {

    protected $graph;

    public function __construct() {
        $this->graph = array();
    }

    public function buildGraph($em) {

        $graph = array();
        $notes = $em->getRepository('GlasgowNotesBundle:Note')->findAll();

        foreach ($notes as $note) {
            if (!array_key_exists(strtoupper($note->getUser()->getUsername()), $graph)) {
                $graph[strtoupper($note->getUser()->getUsername())] = array();
            }

            /* check for relation ship */
            if (strlen($note->getAuthors()) > 1) {
                $no_space = str_replace(' ', '', $note->getAuthors());
                $tmp = explode(',', $no_space);

                foreach ($tmp as $value) {

                    if (!array_key_exists(strtoupper($value), $graph)) {
                        $graph[strtoupper($value)] = array();
                    }

                    if (!in_array(strtoupper($value), $graph[strtoupper($note->getUser()->getUsername())])) {
                        $graph[strtoupper($note->getUser()->getUsername())][] = strtoupper($value);
                    }
                    if (!in_array(strtoupper($note->getUser()->getUsername()), $graph[strtoupper($value)])) {
                        $graph[strtoupper($value)][] = strtoupper($note->getUser()->getUsername());
                    }
                }
            }
        }

        $this->graph = $graph;
    }

    public function getGraph() {
        return $this->graph;
    }

    public function setGraph($graph) {
        $this->graph = $graph;
    }

}
