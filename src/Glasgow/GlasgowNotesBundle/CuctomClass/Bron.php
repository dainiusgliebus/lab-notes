<?php

namespace Glasgow\GlasgowNotesBundle\CuctomClass;

class Bron {
    private $mc = array();

    function bron_kerbosch($graph, $r, $p, $x) {

        if (count($p) == 0 && count($x) == 0) {
            $this->mc[] = $r;
        } else {
            $pivot = $this->select_pivot($graph, $p);
            $i = count($p);
            while ($i > 0) {
                $i--;
                $v = array_shift($p);
                if (!in_array($v, $graph[$pivot])) {
                    $this->bron_kerbosch($graph, array_merge($r, array($v)), array_intersect($p, $graph[$v]), array_intersect($x, $graph[$v]));
                    array_push($x, $v);
                } else {
                    array_push($p, $v);
                }
            }
        }
    }

    function select_pivot($graph, $p) {
        $pivot = -1;
        $max = -1;
        foreach ($p as $vertex) {
            $degree = count($graph[$vertex]);
            if ($degree > $max) {
                $max = $degree;
                $pivot = $vertex;
            }
        }
        return $pivot;
    }
    
    function getMaxClique(){
        
        $max = -1;
        foreach ($this->mc as $k => $v){
            if(count($v) > $max){
                $max = count($v);
                $position = $k;
            }
        }

        return $this->mc[$position];
    }
    
    public function getMc() {
        return $this->mc;
    }

    public function setMc($mc) {
        $this->mc = $mc;
    }



}
