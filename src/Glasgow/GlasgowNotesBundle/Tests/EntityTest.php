<?php

namespace Newstel\EatfishBundle\Tests\Entity;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Glasgow\GlasgowNotesBundle\Entity\Note;
use Glasgow\GlasgowNotesBundle\Entity\Attachment;
use Glasgow\GlasgowNotesBundle\Entity\Mail;

class EntityTest extends KernelTestCase {

  /**
   * @var \Doctrine\ORM\EntityManager
   */
  private $em;
  private $t;

  /**
   * {@inheritDoc}
   */
  public function setUp() {
    self::bootKernel();
    $this->em = static::$kernel->getContainer()->get('doctrine')->getManager();
    $this->t = time();
  }

  public function testNewEditTerminateNote() {
    $user = $this->em->getRepository('GlasgowNotesBundle:User')->find(1);
    $t = $this->t;

    // creating new note
    $note = new Note();
    $note->setTitle("test_" . $t);
    $note->setAbstract("test_" . $t);
    $note->setAuthors("test_" . $t);
    $note->setKeywords("test_" . $t);
    $note->setViews(1);
    $note->setUser($user);
    $note->setNoteRreferences(1);

    $this->em->persist($note);
    $this->em->flush();
    $this->assertEquals(1, count($note));

    // updating note
    $t = time() + 5;
    $note->setTitle("test_" . $t);
    $note->setAbstract("test_" . $t);
    $note->setAuthors("test_" . $t);
    $note->setKeywords("test_" . $t);

    $this->em->persist($note);
    $this->em->flush();
    $this->assertEquals(1, count($note));

    // removing note
    $this->em->remove($note);
    $this->em->flush();
  }

  public function testAddRemoveAttachment() {
    $attachment = new Attachment();
    $note = $this->em->getRepository('GlasgowNotesBundle:Note')->find(1);

    $attachment->setNote($note);
    $attachment->setAttachment("test_1.jpg");
    $this->em->persist($attachment);
    $this->em->flush();
    $this->assertEquals(1, count($attachment));

    // remove attachment
    $this->em->remove($attachment);
    $this->em->flush();
    $this->assertEquals(1, count($attachment));
  }

  public function testMailBox() {
    $mail = new Mail();
    $user = $this->em->getRepository('GlasgowNotesBundle:User')->find(1);
    $mail->setSeen(0);
    $mail->setReceiver($user);
    $mail->setSender($user);
    $mail->setMessage("test mail");

    $this->assertEquals(1, count($mail));

    // remove mail
    $this->em->remove($mail);
    $this->em->flush();
    $this->assertEquals(1, count($mail));
  }

  public function testSearch() {
    $search = "test";
    $splitedSearch = explode(" ", $search);
    if (empty($splitedSearch[1])) {
      $splitedSearch[1] = $splitedSearch[0];
    }

    $query = $this->em->createQuery(
                    'SELECT p
                      FROM GlasgowNotesBundle:Note p
                      WHERE p.title LIKE :title OR p.keywords LIKE :keywords OR p.id = :id
                      OR p.user IN (SELECT u.id FROM GlasgowNotesBundle:User u WHERE u.username LIKE :username)
                      OR p.title LIKE :title01 OR p.keywords LIKE :keywords01
                      OR p.title LIKE :title02 OR p.keywords LIKE :keywords02
                      ORDER BY p.views DESC
                      '
            )
            ->setParameter('title', '%' . $search . '%')
            ->setParameter('keywords', '%' . $search . '%')
            ->setParameter('id', $search)
            ->setParameter('username', '%' . $search . '%')
            ->setParameter('title01', '%' . $splitedSearch[0] . '%')
            ->setParameter('title02', '%' . $splitedSearch[1] . '%')
            ->setParameter('keywords01', '%' . $splitedSearch[0] . '%')
            ->setParameter('keywords02', '%' . $splitedSearch[1] . '%')
            ->setMaxResults(10);
    
    $notes = $query->getResult();

    $this->assertGreaterThan(1, count($notes));
  }

  /**
   * {@inheritDoc}
   */
  protected function tearDown() {
    parent::tearDown();
    $this->em->close();
  }

}
