<?php

namespace Glasgow\GlasgowNotesBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ControllerTest extends WebTestCase {

    protected $timestamp = null;
    protected $client = null;

    public function setUp() {
        $this->timestamp = time();
        //$this->client = $this->createAuthorizedClient();
    }

    public function testUserRegistration() {
        $client = static::createClient();
        $client->insulate();
        $client->followRedirects();
        $t = time();

        $crawler = $client->request('GET', '/register/');
        $form = $crawler->selectButton('registration')->form();

        $form['fos_user_registration_form[email]'] = "test_email_" . $this->timestamp . '@test.com';
        $form['fos_user_registration_form[username]'] = "test_user_" . $this->timestamp;
        $form['fos_user_registration_form[plainPassword][first]'] = "test__pw_" . $this->timestamp;
        $form['fos_user_registration_form[plainPassword][second]'] = "test__pw_" . $this->timestamp;
        $crawler = $client->submit($form);

        $content = $client->getResponse()->getContent();
        $this->assertContains("Latest notes", $content);

        $crawler = $client->request('GET', '/profile/edit/');

        $form = null;

        $form = $crawler->selectButton('edit_user[save]')->form();
        $form['edit_user[username]'] = "test_user_" . $t;
        $form['edit_user[email]'] = "test_email_" . $t . '@test.com';
        $crawler = $client->submit($form);

        $content = $client->getResponse()->getContent();
        $this->assertContains("username: " . ucfirst("test_user_" . $t), $content);
        $this->assertContains("email: " . "test_email_" . $t . '@test.com', $content);
    }

    public function testAddingEditingTerminatingNote() {
        $client = static::createClient();
        $client->insulate();
        $client->followRedirects();
        $t = time();

        $crawler = $client->request('GET', '/login');

        $form = $crawler->selectButton('_submit')->form();
        $form['_username'] = "dainius";
        $form['_password'] = "Kodelciukas123";
        $form['_remember_me']->tick();
        $crawler = $client->submit($form);

        $content = $client->getResponse()->getContent();
        $this->assertContains("Latest notes", $content);

        $crawler = $client->request('GET', '/new-note');

        $form = null;

        $form = $crawler->selectButton('new_note[save]')->form();
        $form['new_note[title]'] = "test_" . $this->timestamp;
        $form['new_note[abstract]'] = "test_" . $this->timestamp;
        $form['new_note[noteRreferences]'] = "1, 2";
        $form['new_note[keywords]'] = "test_" . $this->timestamp . ", test2_" . $this->timestamp;
        $form['new_note[authors]'] = "martynas, saulius";
        $crawler = $client->submit($form);

        $content = $client->getResponse()->getContent();
        // checking if its next page
        $this->assertContains("test_" . $this->timestamp, $content);

        // Note edit test
        $link = $crawler->filter('a:contains("Edit")') // find all links with the text "Edit"
                ->eq(1) // select the second link in the list
                ->link() // and click it
        ;

        $crawler = $client->click($link);

        $form = null;

        $t = time();

        $form = $crawler->selectButton('note[save]')->form();
        $form['note[title]'] = "test_" . $t;
        $form['note[abstract]'] = "test_" . $t;
        $form['note[noteRreferences]'] = "1, 2";
        $form['note[keywords]'] = "test_" . $t . ", test2_" . $t;
        $crawler = $client->submit($form);

        $content = $client->getResponse()->getContent();
        // checking if its next page
        $this->assertContains("test_" . $t, $content);

        //Note termination test

        //$link = $crawler->filter('a:contains("Terminate")') // find all links with the text "Edit"
       //         ->eq(1) // select the second link in the list
        //        ->link() // and click it
        //;
        //$crawler = $client->click($link);

        //$content = $client->getResponse()->getContent();
        // checking if its next page
        //$this->assertContains("Latest notes", $content);
    }

    public function testSearchNoteWithId() {
        $client = static::createClient();
        $client->insulate();
        $client->followRedirects();
        $t = time();

        $crawler = $client->request('GET', '/login');

        $form = $crawler->selectButton('_submit')->form();
        $form['_username'] = "dainius";
        $form['_password'] = "Kodelciukas123";
        $form['_remember_me']->tick();
        $crawler = $client->submit($form);

        $content = $client->getResponse()->getContent();
        $this->assertContains("Latest notes", $content);

        $form = null;
        $form = $crawler->selectButton('_search')->form();
        $form['search'] = "#1";
        $crawler = $client->submit($form);

        $content = $client->getResponse()->getContent();
        $this->assertContains("#1", $content);
    }

    public function testStatistics() {
        $client = static::createClient();
        $client->insulate();
        $client->followRedirects();
        $t = time();

        $crawler = $client->request('GET', '/login');

        $form = $crawler->selectButton('_submit')->form();
        $form['_username'] = "dainius";
        $form['_password'] = "Kodelciukas123";
        $form['_remember_me']->tick();
        $crawler = $client->submit($form);

        $content = $client->getResponse()->getContent();
        $this->assertContains("Latest notes", $content);

        $crawler = $client->request('GET', '/statistics');

        $form = null;
        $form = $crawler->selectButton('displayStats')->form();
        $form['user'] = 1;
        $form['startDate'] = "2015-01-01";
        $form['endDate'] = date("Y-m-d", time());
        $crawler = $client->submit($form);

        $content = $client->getResponse()->getContent();
        $this->assertContains("['months', 'Notes']", $content);
    }

    public function testPageLoading() {
        $client = static::createClient();
        $client->insulate();
        $client->followRedirects();
        $t = time();

        $crawler = $client->request('GET', '/login');

        $form = $crawler->selectButton('_submit')->form();
        $form['_username'] = "dainius";
        $form['_password'] = "Kodelciukas123";
        $form['_remember_me']->tick();
        $crawler = $client->submit($form);

        $content = $client->getResponse()->getContent();
        $this->assertContains("Latest notes", $content);

        $crawler = $client->request('GET', '/new-note');
        $content = $client->getResponse()->getContent();
        $this->assertContains("New note", $content);

        $crawler = $client->request('GET', '/browse');
        $content = $client->getResponse()->getContent();
        $this->assertContains("Found results", $content);

        $crawler = $client->request('GET', '/profile/edit/');
        $content = $client->getResponse()->getContent();
        $this->assertContains("Profile Image", $content);
        $this->assertContains("Username", $content);
        $this->assertContains("Email", $content);

        $crawler = $client->request('GET', '/messages');
        $content = $client->getResponse()->getContent();
        $this->assertContains("Mail Box", $content);
    }

    protected function createAuthorizedClient() {
        $client = static::createClient();
        $container = $client->getContainer();

        $session = $container->get('session');
        /** @var $userManager \FOS\UserBundle\Doctrine\UserManager */
        $userManager = $container->get('fos_user.user_manager');
        /** @var $loginManager \FOS\UserBundle\Security\LoginManager */
        $loginManager = $container->get('fos_user.security.login_manager');
        $firewallName = $container->getParameter('fos_user.firewall_name');

        $user = $userManager->findUserBy(array('username' => 'REPLACE_WITH_YOUR_TEST_USERNAME'));
        $loginManager->loginUser($firewallName, $user);

        // save the login token into the session and put it in a cookie
        $container->get('session')->set('_security_' . $firewallName, serialize($container->get('security.context')->getToken()));
        $container->get('session')->save();
        $client->getCookieJar()->set(new Cookie($session->getName(), $session->getId()));

        return $client;
    }

}
