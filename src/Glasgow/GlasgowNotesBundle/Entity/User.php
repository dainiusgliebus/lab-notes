<?php

namespace Glasgow\GlasgowNotesBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity
 * @ORM\Table(name="gla_user")
 */
class User extends BaseUser {

  /**
   * @ORM\Id
   * @ORM\Column(type="integer")
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  protected $id;

  /**
   * @ORM\OneToMany(targetEntity="Note", mappedBy="user")
   */
  protected $notes;
  
  /**
   * @ORM\OneToMany(targetEntity="Mail", mappedBy="user")
   */
  protected $sender;
  
  /**
   * @ORM\OneToMany(targetEntity="Mail", mappedBy="user")
   */
  protected $receiver;
  

  /**
   * @ORM\Column(type="string", length=255, nullable=true)
   */
  protected $attachment;

  /**
   * @Assert\File(maxSize="6000000")
   */
  private $file;

  function getAttachment() {
    return $this->attachment;
  }

  function getFile() {
    return $this->file;
  }

  function setAttachment($attachment) {
    $this->attachment = $attachment;
  }

  public function setFile(UploadedFile $file = null) {
    $this->file = $file;
  }

  public function getAbsolutePath() {
    return null === $this->attachment ? null : $this->getUploadRootDir() . '/' . $this->attachment;
  }

  public function getWebPath() {
    return null === $this->attachment ? null : $this->getUploadDir() . '/' . $this->attachment;
  }

  protected function getUploadRootDir() {
    // the absolute directory path where uploaded
    // documents should be saved
    return __DIR__ . '/../../../../web/' . $this->getUploadDir();
  }

  protected function getUploadDir() {
    // get rid of the __DIR__ so it doesn't screw up
    // when displaying uploaded doc/image in the view.
    return 'uploads/users';
  }

  public function upload() {
    // the file property can be empty if the field is not required
    if (null === $this->getFile()) {
      return;
    }

    // use the original file name here but you should
    // sanitize it at least to avoid any security issues
    // move takes the target directory and then the
    // target filename to move to
    $filename = $this->username . '.' . $this->extension($this->getFile()->getClientOriginalName());
    $this->getFile()->move($this->getUploadRootDir(), $filename //$this->getFile()->getClientOriginalName()
    );

    // set the path property to the filename where you've saved the file
    $this->attachment = $filename; //$this->getFile()->getClientOriginalName();
    // clean up the file property as you won't need it anymore
    $this->file = null;
  }

  /**
   * Get image
   *
   * @return string 
   */
  public function getImage() {
    return $this->image;
  }

  protected function extension($string) {
    $tmp = explode('.', $string);
    return $tmp[1];
  }

  public function __construct() {
    parent::__construct();

    $this->notes = new ArrayCollection();
    $this->receiver = new ArrayCollection();
    $this->sender = new ArrayCollection();
  }

}
