<?php

// src/Glasgow/GlasgowNotesBundle/Entity/Attachment.php

namespace Glasgow\GlasgowNotesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity
 * @ORM\Table(name="attachments")
 */
class Attachment {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Note", inversedBy="attachment")
     * @ORM\JoinColumn(name="note_id", referencedColumnName="id")
     */
    protected $note;

    /**
     * @ORM\Column(type="datetime", name="created_at")
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $attachment;

    /**
     * @Assert\File(maxSize="6000000000")
     */
    private $file;
    
    private $count;
    
    public function getCount() {
        return $this->count;
    }

    public function setCount($count) {
        $this->count = $count;
    }

    public function getId() {
        return $this->id;
    }

    public function getNote() {
        return $this->note;
    }

    public function getCreatedAt() {
        return $this->createdAt;
    }

    public function getAttachment() {
        return $this->attachment;
    }

    public function getFile() {
        return $this->file;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setNote($note) {
        $this->note = $note;
    }

    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;
    }

    public function setAttachment($attachment) {
        $this->attachment = $attachment;
    }

    public function setFile(UploadedFile $file = null) {
        $this->file = $file;
    }

    public function getAbsolutePath() {
        return null === $this->attachment ? null : $this->getUploadRootDir() . '/' . $this->attachment;
    }

    public function getWebPath() {
        return null === $this->attachment ? null : $this->getUploadDir() . '/' . $this->attachment;
    }

    protected function getUploadRootDir() {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    protected function getUploadDir() {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/documents';
    }

    public function upload() {
        // the file property can be empty if the field is not required
        if (null === $this->getFile()) {
            return;
        }

        // use the original file name here but you should
        // sanitize it at least to avoid any security issues
        // move takes the target directory and then the
        // target filename to move to
        $t=time();
        $filename = $t.'_'.$this->count.'.'.$this->extension($this->getFile()->getClientOriginalName());
        $this->getFile()->move($this->getUploadRootDir(), $filename //$this->getFile()->getClientOriginalName()
        );

        // set the path property to the filename where you've saved the file
        $this->attachment = $filename; //$this->getFile()->getClientOriginalName();
        // clean up the file property as you won't need it anymore
        $this->file = null;
    }
    
    public function remove(){
        unlink($this->getAbsolutePath());
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage() {
        return $this->image;
    }

    protected function extension($string) {
        $tmp = explode('.', $string);
        return $tmp[1];
    }

    function __construct() {

        $this->createdAt = new \DateTime();
    }

}
