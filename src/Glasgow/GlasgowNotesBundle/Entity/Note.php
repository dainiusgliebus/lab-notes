<?php

// src/Glasgow/GlasgowNotesBundle/Entity/Notes.php

namespace Glasgow\GlasgowNotesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity
 * @ORM\Table(name="note")
 */
class Note {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $title;

    /**
     * @ORM\Column(type="integer")
     */
    protected $views;

    /**
     * @ORM\Column(type="text")
     */
    protected $abstract;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $noteRreferences = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $authors = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $keywords = null;

    /**
     * @ORM\Column(type="datetime", name="created_at")
     */
    protected $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="notes")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;
    
    /**
   * @ORM\OneToMany(targetEntity="Attachment", mappedBy="note")
   */
  protected $attachment;
  
  public function getAttachment() {
      return $this->attachment;
  }

  public function setAttachment($attachment) {
      $this->attachment = $attachment;
  }

  
    public function getId() {
        return $this->id;
    }

    public function getTitle() {
        return $this->title;
    }

    public function getViews() {
        return $this->views;
    }

    public function getAbstract() {
        return $this->abstract;
    }

    public function getNoteRreferences() {
        return $this->noteRreferences;
    }

    public function getAuthors() {
        return $this->authors;
    }

    public function getKeywords() {
        return $this->keywords;
    }

    public function getCreatedAt() {
        return $this->createdAt;
    }

    public function getUser() {
        return $this->user;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setTitle($title) {
        $this->title = $title;
    }

    public function setViews($views) {
        $this->views = $views;
    }

    public function setAbstract($abstract) {
        $this->abstract = $abstract;
    }

    public function setNoteRreferences($noteRreferences) {
        $this->noteRreferences = $noteRreferences;
    }

    public function setAuthors($authors) {
        $this->authors = $authors;
    }

    public function setKeywords($keywords) {
        $this->keywords = $keywords;
    }

    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;
    }

    public function setUser($user) {
        $this->user = $user;
    }

    function __construct() {
        $this->views = 0;
        $this->createdAt = new \DateTime();
        
        $this->attachment = new ArrayCollection();
    }

}
