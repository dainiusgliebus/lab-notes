<?php

namespace Glasgow\GlasgowNotesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Glasgow\GlasgowNotesBundle\Entity\Note;
use Glasgow\GlasgowNotesBundle\Entity\Attachment;
use Glasgow\GlasgowNotesBundle\Entity\Mail;
use Glasgow\GlasgowNotesBundle\Form\Type\NoteType;
use Glasgow\GlasgowNotesBundle\Form\Type\NewNoteType;

class NoteController extends Controller {

    public function indexAction() {

        $latestNotesFull = $this->getDoctrine()
                ->getRepository('GlasgowNotesBundle:Note')
                ->findBy([], ['createdAt' => 'DESC'], 10, 0);

        return $this->render('GlasgowNotesBundle:Default:index.html.twig', array(
                    'latestNotesFull' => $latestNotesFull
        ));
    }

    public function newNoteAction(Request $request) {

        $em = $this->getDoctrine()->getManager();

        $note = new Note();

        $form = $this->createForm(new NewNoteType);
        $form->handleRequest($request);

        /* validating form data */
        if ($form->isValid()) {

            $request = $this->get('request');
            $data = $request->request->all();
            //exit(\Doctrine\Common\Util\Debug::dump($data));
            $data = $data['new_note'];

            $note->setUser($this->getUser());
            $createdAt = new \DateTime();
            $note->setCreatedAt($createdAt);

            $note->setAbstract($data['abstract']);
            $note->setAuthors($data['authors']);
            $note->setKeywords($data['keywords']);

            if (strlen(preg_replace("/[^0-9,.]/", "", $data['noteRreferences'])) > 1) {
                $note->setNoteRreferences(preg_replace("/[^0-9,.]/", "", $data['noteRreferences']));
            }

            $note->setTitle($data['title']);

            /* adding data to database */
            $em->persist($note);
            $em->flush();

            $formData = $form->getData();
            $files = $formData["files"];
            //exit(\Doctrine\Common\Util\Debug::dump($files));

            $i = 0;
            foreach ($files as $file) {
                if ($file <> null) {
                    $attachment = new Attachment();
                    $attachment->setNote($note);
                    $attachment->setFile($file);
                    $attachment->setCount($i);
                    $attachment->upload();

                    $em->persist($attachment);
                    $em->flush();
                    $i++;
                }
            }


            if (!empty($data['notify'])) {

                $usersToNotify = $data['notify'];

                foreach ($usersToNotify as $value) {
                    $user = $this->getDoctrine()->getRepository('GlasgowNotesBundle:User')->find($value);

                    if ($user->getId() != $note->getUser()->getId()) {

                        $mail = new Mail();
                        $mail->setSender($note->getUser());
                        $mail->setReceiver($user);

                        $mail->setMessage("User: " . $note->getUser()->getUsername() . ' wants to notify you about his new note #' . $note->getId());

                        //exit(\Doctrine\Common\Util\Debug::dump($mail));
                        $em->persist($mail);
                        $em->flush();
                    }
                }
            }

            return $this->redirect($this->generateUrl('glasgow_notes_view_note', array('id' => $note->getId())));
        }

        return $this->render('GlasgowNotesBundle:Default:new-note.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    public function editNoteAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('GlasgowNotesBundle:Note');
        $note = $repository->find($request->get('id'));

        $user = $this->getUser();

        $form = $this->createForm(new NoteType, $note);
        $form->handleRequest($request);

        /* validating form data */
        if ($form->isValid()) {

            $note = $form->getData();

            /* adding data to database */

            $em->persist($note);
            $em->flush();


            $files = $form->get('files')->getData();
            //exit(\Doctrine\Common\Util\Debug::dump($files));

            $i = 0;
            foreach ($files as $file) {
                if ($file <> null) {
                    $attachment = new Attachment();
                    $attachment->setNote($note);
                    $attachment->setFile($file);
                    $attachment->setCount($i);
                    $attachment->upload();

                    $em->persist($attachment);
                    $em->flush();
                    $i++;
                }
            }
            return $this->redirect($this->generateUrl('glasgow_notes_view_note', array('id' => $note->getId())));
        }

        return $this->render('GlasgowNotesBundle:Note:edit.html.twig', array(
                    'form' => $form->createView(),
                    'note' => $note
        ));
    }

    public function viewNoteAction(Request $request, $id) {

        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('GlasgowNotesBundle:Note');
        $note = $repository->find($id);

        $repository = $em->getRepository('GlasgowNotesBundle:Attachment');
        $attachments = $repository->findByNote($note);

        $edit = 0;

        $user = $this->getUser();

        if ($user->getId() == $note->getUser()->getId()) {
            $edit = 1;
        }

        $note->setViews($note->getViews() + 1);
        $em->persist($note);
        $em->flush();

        $rel = null;
        if ($note->getNoteRreferences() <> '') {
            $no_space = str_replace(' ', '', $note->getNoteRreferences());
            $tmp = explode(',', $no_space);

            foreach ($tmp as $value) {
                $repository = $em->getRepository('GlasgowNotesBundle:Note');
                $rel[] = $repository->find(trim($value));
            }
        }

        $query = $em->createQuery('SELECT n FROM GlasgowNotesBundle:Note n WHERE n.noteRreferences LIKE :first OR n.noteRreferences LIKE :second OR n.noteRreferences LIKE :third')
                ->setParameter('first', $id . '%')
                ->setParameter('second', '%,' . $id . ',%')
                ->setParameter('third', '%,' . $id);


        $notesToRefer = $query->getResult();
        foreach ($notesToRefer as $notesToRef) {
            $rel[] = $notesToRef;
        }

        $keywords = null;
        if ($note->getKeywords() <> '') {
            //$no_space = str_replace(' ', '', $note->getNoteRreferences());
            $tmp = explode(',', $note->getKeywords());

            foreach ($tmp as $value) {
                $keywords[] = trim($value);
            }
        }

        $graph = $this->createGraph($id);

        //exit(\Doctrine\Common\Util\Debug::dump($relatedList));
        return $this->render('GlasgowNotesBundle:Note:note.html.twig', array(
                    'note' => $note,
                    'edit' => $edit,
                    'keywords' => $keywords,
                    'graph' => $graph,
                    'attachments' => $attachments,
                    'relNote' => $rel
        ));
    }

    public function removeFileAction($id) {

        $em = $this->getDoctrine()->getManager();

        $user = $this->getUser();

        $repository = $em->getRepository('GlasgowNotesBundle:Attachment');
        $original = $repository->find($id);

        $query = $em->createQuery(
                        'SELECT a
                    FROM GlasgowNotesBundle:Attachment a, GlasgowNotesBundle:User u, GlasgowNotesBundle:Note n
                    WHERE u.id = :user AND u.id = n.user AND n.id = a.note AND a.id = :file'
                )->setParameter('file', $id)
                ->setParameter('user', $user);

        $attachments = $query->getResult();
        //exit(\Doctrine\Common\Util\Debug::dump($attachments));

        foreach ($attachments as $attachment) {
            $em->remove($attachment);
            $em->flush();

            $attachment->remove();
        }
        return $this->redirect($this->generateUrl('glasgow_notes_view_note', array('id' => $original->getNote()->getId())));
    }

    public function deleteAction($id) {

        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('GlasgowNotesBundle:Note');
        $note = $repository->find($id);

        $repository = $em->getRepository('GlasgowNotesBundle:Attachment');
        $files = $repository->findByNote($id);
        
        foreach ($files as $attachment) {
            $em->remove($attachment);
            $em->flush();

            $attachment->remove();
        }

        $user = $this->getUser();

        if ($note->getUser()->getId() == $user->getId()) {
            $em->remove($note);
            $em->flush();

            return $this->redirect($this->generateUrl('glasgow_notes_homepage'));
        } else {
            return $this->redirect($this->generateUrl('glasgow_notes_view_note', array('id' => $id)));
        }
    }

    public function createGraph($id) {
        $list = array($id);
        $checked = array();
        $graph = array();

        while (!empty($list)) {
            $checked[] = $list[0];
            $cid = array_pop($list);

            $note = $this->getDoctrine()->getRepository('GlasgowNotesBundle:Note')->find($cid);

            if ($note->getNoteRreferences() <> '') {
                $no_space = str_replace(' ', '', $note->getNoteRreferences());
                $tmp = explode(',', $no_space);

                $graph[$note->getId()] = $tmp;

                foreach ($tmp as $value) {

                    if (!in_array(trim($value), $list)) {
                        $list[] = trim($value);
                    }
                }
                sort($list);
            } else {
                $graph[$note->getId()] = '';
            }
        }

        return $graph;
    }

}
