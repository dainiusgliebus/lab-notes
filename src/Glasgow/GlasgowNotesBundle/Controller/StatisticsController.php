<?php

namespace Glasgow\GlasgowNotesBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Glasgow\GlasgowNotesBundle\CuctomClass\Graph;
use Glasgow\GlasgowNotesBundle\CuctomClass\Bron;

class StatisticsController extends Controller {
    
    private $mc = array();
    private $names = array();

    public function indexAction(Request $request, $user, $startDate, $endDate) {

        $em = $this->getDoctrine()->getManager();
        $usersList = $this->getDoctrine()->getRepository('GlasgowNotesBundle:User')->findAll();
        $users = $this->getDoctrine()->getRepository('GlasgowNotesBundle:User')->findAll();
        $type = 'months';

        $startDateSelected = '2014-01-01';
        $endDateSelected = date('Y-m-d');
        $userSelected = 'all';

        if ($startDate == 'default') {
            $time = new \Datetime();
            $startDate = $time->modify('-1 year')->format('Y-m-d');
            $endDate = new \Datetime();
        }

        if ($request->isMethod('POST')) {

            //$request = $this->get('request');
            $data = $request->request->all();

            $startDateSelected = $data['startDate'];
            $endDateSelected = $data['endDate'];

            if (substr($data['startDate'], 0, 7) == substr($data['endDate'], 0, 7)) {
                $type = 'days';
            }

            $startDate = new \Datetime();
            $startDate->setDate(substr($data['startDate'], 0, 4), substr($data['startDate'], 5, 2), substr($data['startDate'], 8, 2));

            $endDate = new \Datetime();
            $endDate->setDate(substr($data['endDate'], 0, 4), substr($data['endDate'], 5, 2), substr($data['endDate'], 8, 2));

            $user = $data['user'];
            if ($user <> 'all') {
                $users = $this->getDoctrine()->getRepository('GlasgowNotesBundle:User')->find($user);
                $userSelected = $user;
            }
            //exit(\Doctrine\Common\Util\Debug::dump($data));
        }

        $query = $em->createQuery(
                        'SELECT n
                    FROM GlasgowNotesBundle:Note n
                    WHERE n.createdAt > :startime AND n.createdAt < :endtime AND n.user IN (:users) 
                    ORDER BY n.createdAt ASC'
                )->setParameter('startime', $startDate)
                ->setParameter('endtime', $endDate)
                ->setParameter('users', $users);

        $notes = $query->getResult();

        $totalRows = count($notes);
        if ($totalRows < 1) {

            $users = $this->getDoctrine()->getRepository('GlasgowNotesBundle:User')->findAll();
            $time = new \Datetime();
            $startDate = $time->modify('-1 year')->format('Y-m-d');
            $endDate = new \Datetime();

            $query = $em->createQuery(
                            'SELECT n
                    FROM GlasgowNotesBundle:Note n
                    WHERE n.createdAt > :startime AND n.createdAt < :endtime AND n.user IN (:users) 
                    ORDER BY n.createdAt ASC'
                    )->setParameter('startime', $startDate)
                    ->setParameter('endtime', $endDate)
                    ->setParameter('users', $users);

            $notes = $query->getResult();
        }

        $stats = null;

        foreach ($notes as $note) {
            if ($type == 'months') {
                $ym = $note->getCreatedAt()->format('Y-m');
            } else if ($type == 'days') {
                $ym = $note->getCreatedAt()->format('d');
            }

            if (!isset($stats[$ym])) {
                $stats[$ym] = 0;
            } else {
                $stats[$ym] = $stats[$ym] + 1;
            }
        }

        ksort($stats);
        //exit(\Doctrine\Common\Util\Debug::dump($stats));
        
        $graph = new Graph();
        $graph->buildGraph($em);
        $newGraph = $graph->getGraph();
        
        $bron = new Bron();
        $bron->bron_kerbosch($newGraph, array(), array_keys($newGraph), array());
        $maxClique = $bron->getMaxClique();

        return $this->render('GlasgowNotesBundle:Statistics:index.html.twig', array(
                    'stats' => $stats,
                    'maxClique' => $maxClique,
                    'type' => $type,
                    'users' => $users,
                    'usersList' => $usersList,
                    'selectUser' => $userSelected,
                    'selectStartDate' => $startDateSelected,
                    'selectEndDate' => $endDateSelected,
        ));
    }

}
