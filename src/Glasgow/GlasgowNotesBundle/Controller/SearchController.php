<?php

namespace Glasgow\GlasgowNotesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class SearchController extends Controller {

    public function indexAction($page) {

        $em = $this->getDoctrine()->getManager();

        $repository = $em->getRepository('GlasgowNotesBundle:Note');
        $notes = $repository->findAll();
        $total = count($notes);

        $number = ceil($total / 10);

        $position = $page * 10;

        $notes = $repository->findBy(array(), array("createdAt" => "DESC"), 10, $position - 10);

        return $this->render('GlasgowNotesBundle:Search:index.html.twig', array(
                    'notes' => $notes,
                    'total' => $total,
                    'pages' => $number,
                    'currentPage' => $page
        ));
    }

    public function searchAction(Request $request) {

        $request = $this->get('request');

        if ($request->getMethod() == 'GET') {

            $em = $this->getDoctrine()->getManager();
            $repository = $em->getRepository('GlasgowNotesBundle:Note');


            $search = $request->query->get('search');
            

            if (strpos($search, '#') !== false) {
                $search = str_replace('#', '', $search);
                $repository = $em->getRepository('GlasgowNotesBundle:Note');
                $note = $repository->find($search);

                if (count($note) == 1) {
                    return $this->redirect($this->generateUrl('glasgow_notes_view_note', array('id' => $note->getId())));
                }
            }

            //exit(\Doctrine\Common\Util\Debug::dump($search));
            
            $splitedSearch = explode (" ", $search);
            if(empty($splitedSearch[1])){
                $splitedSearch[1] = $splitedSearch[0];
            }
            
            $page = 1;
            
            $req = $request->query->get('page');
            if(!empty($req)){
                $page = $req;
            }
            $position = $page * 10;

            $query = $em->createQuery(
                            'SELECT p
                      FROM GlasgowNotesBundle:Note p
                      WHERE p.title LIKE :title OR p.keywords LIKE :keywords OR p.id = :id
                      OR p.user IN (SELECT u.id FROM GlasgowNotesBundle:User u WHERE u.username LIKE :username)
                      OR p.title LIKE :title01 OR p.keywords LIKE :keywords01
                      OR p.title LIKE :title02 OR p.keywords LIKE :keywords02
                      ORDER BY p.views DESC
                      '
                    )
                    ->setParameter('title', '%' . $search . '%')
                    ->setParameter('keywords', '%' . $search . '%')
                    ->setParameter('id', $search)
                    ->setParameter('username', '%' . $search . '%')
                    ->setParameter('title01', '%' . $splitedSearch[0] . '%')
                    ->setParameter('title02', '%' . $splitedSearch[1] . '%')
                    ->setParameter('keywords01', '%' . $splitedSearch[0] . '%')
                    ->setParameter('keywords02', '%' . $splitedSearch[1] . '%')
                    ->setFirstResult($position - 10)
                    ->setMaxResults(10);


            //exit(\Doctrine\Common\Util\Debug::dump($query));
            $notes = $query->getResult();
            
            $query = $em->createQuery(
                            'SELECT p
                      FROM GlasgowNotesBundle:Note p
                      WHERE p.title LIKE :title OR p.keywords LIKE :keywords OR p.id = :id
                      OR p.user IN (SELECT u.id FROM GlasgowNotesBundle:User u WHERE u.username LIKE :username)
                      OR p.title LIKE :title01 OR p.keywords LIKE :keywords01
                      OR p.title LIKE :title02 OR p.keywords LIKE :keywords02
                      ORDER BY p.views DESC
                      '
                    )
                    ->setParameter('title', '%' . $search . '%')
                    ->setParameter('keywords', '%' . $search . '%')
                    ->setParameter('id', $search)
                    ->setParameter('username', '%' . $search . '%')
                    ->setParameter('title01', '%' . $splitedSearch[0] . '%')
                    ->setParameter('title02', '%' . $splitedSearch[1] . '%')
                    ->setParameter('keywords01', '%' . $splitedSearch[0] . '%')
                    ->setParameter('keywords02', '%' . $splitedSearch[1] . '%');
            $notesTotal = $query->getResult();
            $total = count($notesTotal);
            $number = ceil($total / 10);
            

            //$notes = $repository->findBy(array(), array(), 10, $position - 10);

            return $this->render('GlasgowNotesBundle:Search:search.html.twig', array(
                        'notes' => $notes,
                        'total' => $total,
                        'pages' => $number,
                        'currentPage' => $page,
                        'search' => $search
            ));
        } else {
            return $this->redirect($this->generateUrl('glasgow_notes_homepage'));
        }
    }

}
