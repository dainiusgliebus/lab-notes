<?php

namespace Glasgow\GlasgowNotesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Glasgow\GlasgowNotesBundle\Form\Type\UserType;

class ProfileController extends Controller {

    public function indexAction(Request $request) {
	$stats = null;
        $em = $this->getDoctrine()->getManager();
        $username = $request->get('name');
        $repository = $em->getRepository('GlasgowNotesBundle:User');
        $profile = $repository->findOneBy(array('username' => $username));

        $user = $this->getUser();
        $repository = $em->getRepository('GlasgowNotesBundle:Note');
        $mostViewed = $repository->findBy(array('user' => $profile), array('views' => 'DESC'), 5, 0);

        $repository = $em->getRepository('GlasgowNotesBundle:Note');
        $latestViewed = $repository->findBy(array('user' => $profile), array('createdAt' => 'DESC'), 5, 0);

        $repository = $em->getRepository('GlasgowNotesBundle:Note');
        $totalNotes = $repository->findBy(array('user' => $profile));
        $total = count($totalNotes);

        $edit = 0;
        if ($user->getId() == $profile->getId()) {
            $edit = 1;
        }

        $time = new \Datetime();
        $startDate = $time->modify('-1 year')->format('Y-m-d');
        $endDate = new \Datetime();

        $query = $em->createQuery(
                        'SELECT n
                    FROM GlasgowNotesBundle:Note n
                    WHERE n.createdAt > :startime AND n.createdAt < :endtime AND n.user IN (:users) 
                    ORDER BY n.createdAt ASC'
                )->setParameter('startime', $startDate)
                ->setParameter('endtime', $endDate)
                ->setParameter('users', $profile);

        $notes = $query->getResult();

        foreach ($notes as $note) {
            $month = $note->getCreatedAt()->format('Y-m');

            if(empty($stats[$month])) {
                $stats[$month] = 1;
            }else {
                $stats[$month] = $stats[$month] + 1;
            }
        }

        return $this->render('GlasgowNotesBundle:Profile:index.html.twig', array(
                    'profile' => $profile,
                    'mostViewed' => $mostViewed,
                    'latestViewed' => $latestViewed,
                    'totalNotes' => $total,
                    'edit' => $edit,
                    'stats' => $stats
        ));
    }

    public function editAction(Request $request) {

        $em = $this->getDoctrine()->getManager();

        $user = $this->getUser();

        $form = $this->createForm(new UserType, $user);
        $form->handleRequest($request);
        
        $success = null;

        /* validating form data */
        if ($form->isValid()) {
            $user->upload();
            $em->persist($user);
            $em->flush();
            $success = 1;
        }

        //exit(\Doctrine\Common\Util\Debug::dump($user));
        return $this->render('GlasgowNotesBundle:Profile:edit.html.twig', array(
                    'user' => $user,
                    'form' => $form->createView(),
                    'success' => $success
        ));
    }

}
