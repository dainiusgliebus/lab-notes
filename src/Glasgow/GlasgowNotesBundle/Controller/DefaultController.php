<?php

namespace Glasgow\GlasgowNotesBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller {

    public function indexAction() {

        $latestNotesFull = $this->getDoctrine()
                ->getRepository('GlasgowNotesBundle:Note')
                ->findBy([], ['createdAt' => 'DESC'], 15, 0);

        return $this->render('GlasgowNotesBundle:Default:index.html.twig', array(
                    'latestNotesFull' => $latestNotesFull
        ));
    }

    public function navbarAction() {

        $em = $this->getDoctrine()->getManager();
        $messages = 0;

        $user = $this->getUser();
        if(isset($user)){
        $messages = $em->getRepository('GlasgowNotesBundle:Mail')->findBy(['receiver' => $user, 'seen' => '0']);
        }
        
        return $this->render('GlasgowNotesBundle:Default:navbar.html.twig', array(
                    'messages' => count($messages)
        ));
    }

}
