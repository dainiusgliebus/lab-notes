<?php

namespace Glasgow\GlasgowNotesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MessagesController extends Controller {

    public function listAction($page) {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        $messages = $em->getRepository('GlasgowNotesBundle:Mail')->findBy(['receiver' => $user, 'seen' => '0'], ['createdAt' => 'DESC']);
        
        //exit(\Doctrine\Common\Util\Debug::dump($messages));

        return $this->render('GlasgowNotesBundle:Messages:list.html.twig', array(
                    'messages' => $messages
        ));
    }

    public function hideAction($id) {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        $message = $em->getRepository('GlasgowNotesBundle:Mail')->findOneBy(array('receiver' => $user, 'id' => $id));
        $message->setSeen(1);
        $em->persist($message);
        $em->flush();

        return $this->redirect($this->generateUrl('glasgow_messages'));
    }

}
