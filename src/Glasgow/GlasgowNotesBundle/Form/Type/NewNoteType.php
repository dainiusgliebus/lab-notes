<?php

// src/Glasgow/GlasgowNotesBundle/Form/Type/NewNoteType.php

namespace Glasgow\GlasgowNotesBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class NewNoteType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
                ->add('title', 'text', array(
                    'label' => 'Title',
                    'required' => true,
                    'attr' => array('class' => 'form-control', 'placeholder' => 'Title'),
                    'label_attr' => array('class' => 'control-label')
                ))
                ->add('abstract', 'textarea', array(
                    'label' => 'Abstract',
                    'required' => true,
                    'attr' => array('class' => 'form-control', 'placeholder' => 'Abstract', 'rows' => 7),
                    'label_attr' => array('class' => 'control-label')
                ))
                ->add('noteRreferences', 'text', array(
                    'label' => 'Related notes',
                    'required' => false,
                    'attr' => array('class' => 'form-control', 'placeholder' => 'Related notes (ID separated with comma) Ex.: 1, 2'),
                    'label_attr' => array('class' => 'control-label')
                ))
                ->add('authors', 'text', array(
                    'label' => 'Additional Authors',
                    'required' => false,
                    'attr' => array('class' => 'form-control', 'placeholder' => 'Authors (separated with comma)'),
                    'label_attr' => array('class' => 'control-label')
                ))
                ->add('keywords', 'text', array(
                    'label' => 'Keywords',
                    'required' => false,
                    'attr' => array('class' => 'form-control', 'placeholder' => 'Keywords (separated with comma)'),
                    'label_attr' => array('class' => 'control-label')
                ))
                ->add('files', 'file', array(
                    'label' => 'Files',
                    'required' => false,
                    'data_class' => null,
                    'multiple' => true,
                    'attr' => array('class' => 'form-control'),
                    'label_attr' => array(
                        'class' => 'control-label',
                        'placeholder' => 'Files'
                    )
                ))
                
                ->add('notify', 'entity', array(
                    'class' => 'GlasgowNotesBundle:User',
                    'label' => 'Notify user about this note:',
                    'required' => false,
                    'multiple' => true,
                    'mapped' => false,
                    'attr' => array('class' => 'form-control', 'placeholder' => 'Notify:'),
                    'label_attr' => array('class' => 'control-label')
                ))
                ->add('save', 'submit', array(
                    'label' => 'Save',
                    'attr' => array('class' => 'btn btn-lg btn-success', 'style' => 'margin-top: 10px')
        ));
    }

    public function getName() {
        return 'new_note';
    }

}
