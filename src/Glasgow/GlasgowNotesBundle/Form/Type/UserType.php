<?php

// src/Glasgow/GlasgowNotesBundle/Form/Type/UserType.php

namespace Glasgow\GlasgowNotesBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class UserType extends AbstractType {

  public function buildForm(FormBuilderInterface $builder, array $options) {
    $builder
            ->add('username', 'text', array(
                'label' => 'Username',
                'required' => true,
                'attr' => array('class' => 'form-control', 'placeholder' => 'Username'),
                'label_attr' => array('class' => 'control-label')
            ))
           ->add('email', 'text', array(
                'label' => 'Email',
                'required' => true,
                'attr' => array('class' => 'form-control', 'placeholder' => 'Email'),
                'label_attr' => array('class' => 'control-label')
            ))
            ->add('file', 'file', array(
                'label' => 'Profile Image',
                'required' => false,
                'data_class' => null,
                'attr' => array('class' => 'form-control'),
                'label_attr' => array(
                    'class' => 'control-label',
                    'placeholder' => 'Profile Image'
                )
            ))
            ->add('save', 'submit', array(
                'label' => 'Save',
                'attr' => array('class' => 'btn btn-lg btn-success', 'style' => 'margin-top: 10px')
    ));
  }

  public function getName() {
    return 'edit_user';
  }

}