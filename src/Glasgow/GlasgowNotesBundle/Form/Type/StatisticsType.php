<?php

// src/Glasgow/GlasgowNotesBundle/Form/Type/StatisticsUserType.php

namespace Glasgow\GlasgowNotesBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class StatisticsType extends AbstractType {

  public function buildForm(FormBuilderInterface $builder, array $options) {
    for ($i = 0; $i < 10; $i++) {
      $years[2014 + $i] = 2014 + $i;
    }

    $builder
            ->add('user', 'entity', array(
                'label' => 'User(s)',
                'class' => 'GlasgowNotesBundle:User',
                'multiple' => false,
                'required' => true,
                'attr' => array('class' => 'form-control', 'placeholder' => 'Abstract', 'rows' => 7),
                'label_attr' => array('class' => 'control-label'),
                'data' => 'all'
            ))
            ->add('startDate', 'date', array(
                'input' => 'datetime',
                'widget' => 'choice',
                'required' => true,
                'years' => $years,
            ))
            ->add('endDate', 'date', array(
                'input' => 'datetime',
                'widget' => 'choice',
                'required' => true,
                'years' => $years,
            ))
            ->add('Display', 'submit', array(
                'label' => 'Display',
                'attr' => array('class' => 'btn btn-lg btn-success', 'style' => 'margin-top: 10px')
    ));
  }

  public function getName() {
    return 'statistics';
  }

}
